// specified the state changes in response to actions to context
// a way change the state and send it down to the application

export default (state, action) => {

  switch (action.type) {

    // return a new copy of the state without the one id-ed in the reducer payload
    case 'DELETE_TRANSACTION':
      return {
        ...state,
        transactions: state.transactions.filter(x => x.id !== action.payload)
      }

    case 'ADD_TRANSACTION':
      return {
        ...state,
        transactions: [action.payload, ...state.transactions]
      }

    default:
      return state;
  }
}