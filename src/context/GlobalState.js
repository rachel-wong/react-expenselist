import React, { createContext, useReducer } from 'react';
import AppReducer from './AppReducer'
// context state
// global initial state goes in here
const initialState = {
  transactions: [
    { id: 1, text: 'Flower', amount: -20 },
    { id: 2, text: 'Salary', amount: 300 },
    { id: 3, text: 'Book', amount: -10 },
    { id: 4, text: 'Camera', amount: 150 }
  ]
}

// create global context state and export it to make it usable in other files
export const GlobalContext = createContext(initialState)

// need to create and wrap the App.js with a provider in order for the app itself to access the globalcontext
export const GlobalProvider = ({ children }) => {
  // AppReducer is a list of all of the reducers available to manipulate the global store
  const [state, dispatch] = useReducer(AppReducer, initialState)

  // actions to call reducers
  // declare the action, and pass it down to the App via the GlobalContext.Provider below
  function deleteTransaction(id) {
    // this sent to the switch statement in AppReducer
    dispatch({
      type: 'DELETE_TRANSACTION',
      payload: id
    })
  }

  function addTransaction(transaction) {
    dispatch({
      type: 'ADD_TRANSACTION',
      payload: transaction
    })
  }

  // children is all the components that are wrapped inside the provider inside App.js
  // in the value argument, state is the initialstate object initialised above
  // methods and objects need to pass in via the "value" attribute
  return (<GlobalContext.Provider value={{ transactions: state.transactions, deleteTransaction, addTransaction} } >
      {children}
    </GlobalContext.Provider>
    )
  }