import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'

const Balance = () => {
  const { transactions } = useContext(GlobalContext)
  const total = transactions.map(a => a.amount).reduce((a, b) => a + b, 0)

  return (
    <>
      <h4>Your balance</h4>
      <h1 id="balance">{ total < 0 ? "-$" + Math.abs(total) : "$" + total }</h1>
    </>
  )
}

export default Balance