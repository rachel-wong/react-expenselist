import React, { useContext } from 'react'
import { GlobalContext} from "../context/GlobalState"

const TransactionEntry = ({ id, text, amount }) => {
  const { deleteTransaction } = useContext(GlobalContext) // get whatever it is needed from the global context
  console.log("hello", deleteTransaction)
  return (
    <li className={ amount < 0 ? "minus" : "plus"}>
      {text} <span>{amount < 0 ? "-$" + Math.abs(amount) : "$" + amount}</span>
      <button className="delete-btn" onClick={ () => deleteTransaction(id) }>x</button>
    </li>
  )
}

export default TransactionEntry