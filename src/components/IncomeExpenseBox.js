import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'

const IncomeExpenseBox = () => {
  const { transactions } = useContext(GlobalContext); // access the global state
  const amounts = transactions.map(a => a.amount)
  const expenseTotal = amounts.filter(amount => amount < 0).reduce((a, b) => a + b, 0)
  const incomeTotal = amounts.filter(amount => amount > 0).reduce((a, b) => a + b, 0)

  return (
    <div className="inc-exp-container">
      <div>
        <h4>Income</h4>
        <p id="money-plus" className="money plus">+${ incomeTotal }</p>
      </div>
      <div>
        <h4>Expense</h4>
        <p id="money-minus" className="money minus">-${ Math.abs(expenseTotal) }</p>
      </div>
    </div>
  )
}

export default IncomeExpenseBox
