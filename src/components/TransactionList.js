import React, { useContext } from 'react'
import TransactionEntry from './TransactionEntry'
import { GlobalContext } from '../context/GlobalState'

const TransactionList = () => {

  const { transactions } = useContext(GlobalContext); // access the global state

  return (
    <>
      <h3>History</h3>
      <ul className="list">
        {transactions.map(entry => (
          <TransactionEntry key={entry.id} text={entry.text} amount={entry.amount} id={ entry.id}></TransactionEntry>
        ))}
      </ul>
    </>
  )
}

export default TransactionList